$(function () {

    $('body').scrollspy({
        target: '.navbar-scrollpy'
    })

    var _featuresSection = $('section#features').offset().top - $('.navbar').height();

    // MENU STICK
    $(document).scroll(function (e) {
        var scrollTop = $(this).scrollTop();
        var $nav = $('.menu');
        if (scrollTop >= _featuresSection) {
            if ($nav.hasClass('navbar-fixed-top')) {
                return;
            }
		    
            $nav.removeClass('navbar-static-top').addClass('navbar-fixed-top');
           
           
            $nav.find('.navbar').find('ul.nav li:last-child').show();
            
            
            
            //$('body').addClass('bodymargin');
            setTimeout(function(){  $nav.find('.navbar').addClass('show-down');},50);
            
			/*
            $nav.stop().animate({
                'overflow': 'auto',
                'min-height': 50,
                'height': 50,
            }, 300);
			*/
        } else {

            if ($nav.hasClass('navbar-static-top')) {
                return;
            }
            
            $nav.find('.navbar').removeClass('show-down');
            
            setTimeout(function(){ 
                
                //$('body').removeClass('bodymargin');
                $nav.find('.navbar').find('ul.nav li:last-child').hide();
                $nav.removeClass('navbar-fixed-top').addClass('navbar-static-top'); 
            },200);
            
            
           
            /*
             $nav.stop().animate({
             'overflow': 'hidden',
             'min-height': 0,
             'height': 0,
             }, 200, function () {
             
             $(this).css({
             'overflow': 'auto',
             'min-height': 50,
             'height': 50,
             });
             $(this).removeClass('navbar-fixed-top').addClass('navbar-static-top');
             });
             */

        }
    });

    // SMOOTH SCROLL
    $('a[href*=#]:not([href=#]):not([class="accordion-toggle"])').click(function () {
        if (!$(this).hasClass('fancybox')) {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: (target.offset().top - 49 )
                    }, 500);
                    return false;
                }
            }
        }
    });

    $('.flow-validate').each(function (index) {
        var $input = $(this);
        $input.blur(function () {
            if ($(this).val() != "") {
                $(this).parent().removeClass('has-error');
            }
        });
    });

    // SEND MAIL
    $('.flow-btn-submit').click(function () {

        if ($('.flow-btn-submit').hasClass('mail-success')) {
            return;
        }

        var data = $('form#mail').serialize();
        var isBreak = false;
        $('.flow-validate').each(function (index) {

            var $input = $(this);
            if ($input.val() == "") {
                $input.parent().addClass('has-error');
                $input.focus();
                isBreak = true;
                return false;
            }
        });

        if (isBreak)
            return;

        $('.flow-btn-submit').parents('form').submit();

        //$.ajax({
        //    'cache': false,
        //    'type': 'POST',
        //    'url': 'mail.php',
        //    'data': data
        //}).done(function (result) {
        //    //console.log(result);
        //    $('.input-mail').css({
        //        'overflow': 'hidden'
        //    });

        //    $('.input-mail').animate({
        //        'height': 0,
        //    }, 500, function () {
        //        $('.flow-btn-submit').addClass('mail-success').html('Thankyou');
        //    });
        //});

    });
    
   

    // fancybox
//    $(".fancybox").fancybox({
//        maxWidth: 764,
//        height: 'auto',
//        width: '100%',
//        fitToView: false,
//        padding: 40,
//        autoSize: false,
//        closeClick: false
//       
//        //openEffect : 'fade',
//    });

//    $('#login .btn-close-dialog, #signup .btn-close-dialog').click(function(e){
//           
//                $.fancybox.close();
//     });

    // show invoice page 
//    $('.invoice-container').css({
//        'height': $(window).height(), // '100%', //;
//        'top': -($(window).height())//'-100%', // ;
//    });
    /*$('.btn-toggle-cover').click(function () {

        $('.invoice-container').scrollTop(0);
        $('.invoice-container').toggleClass('slide-down');
        $('body').toggleClass('scroll-hidden');
        $('body').scrollTop(0);
        
        $("body").bind("mousewheel", function () {
           // return;
            var isScroll = true;
            if ($('.invoice-container').hasClass('slide-down')) {
                isScroll = true;

            } else {

            }
            return isScroll;
        });
    });*/
    
    
    
     $('#exportpdfbtn').on('click', function () {


                $('.popover').remove();
                if ($('#invoiceForm').valid()) {
                    var scope = angular.element($(".invoice-page")).scope();
                    scope.open('SaveFreeInvoiceCtrl', 'ModalContent.html');
                }
            });
    
    $('.btn-toggle-cover-close').click(function()
    {
        
         $('.trowser').removeClass('slide-down');
         $('.trowser').toggleClass('slide-up');
         setTimeout(function () {
                    $('.trowser').toggleClass('slide-up');
                    $('.shell').toggleClass('trowserView');
                    $('.trowser').find('.invoice-container .web-view').html('');
                    $('body').css('overflow','auto');
         }

        , 500);
    });
    //var el = document.querySelector('.js-fade');
    //if (el.classList.contains('is-paused')) {
    //    el.classList.remove('is-paused');
    //}
    //else { el.classList.addClass('is-paused');}
    ///* for choose review */
    //$('[name=btn-review]').on('click', function () {
    //    $('[name=btn-review]').removeClass('active');
    //    $(this).addClass('active');
    //    var id = $(this).attr('data-value');
        
    //    $('[name=panel-review]').addClass('hidden');
    //    $('#info-' + id).removeClass('hidden');

    //    $('#ar-' + id).removeClass('hidden');
    //});
    /* end review */

});