/// <reference path="../newDesign/pricing-package.html" />
/// <reference path="../newDesign/pricing-package.html" />
var angularApp;

(function () {
    "use strict";
    //angularApp = angular.module("app", ["ngRoute", "ui.bootstrap", "dialogs.controllers", "dialogs.services", "pascalprecht.translate", "app.localization", "app.frontend", "duScroll","ngCookies","ngBiscuit"]);

    //angularApp = angular.module("app", ["ng","ngCookies","ngBiscuit","ngRoute", "pascalprecht.translate", "ui.bootstrap", "dialogs.controllers", "dialogs.services", "duScroll"]);
   
    angularApp = angular.module("app", ["ngCookies","ngBiscuit","ngRoute", "ngSanitize","ui.bootstrap", "dialogs.main", "app.localization", "app.frontend", "pascalprecht.translate", "duScroll","ui.mask" ]);
    

    angularApp.run(["$rootScope", "$location", "$http", "$timeout", "$templateCache", "$window", "$translate", function ($rootScope, $location, $http, $timeout, $templateCache, $window, $translate) {


        $templateCache.put("/dialogs/notify1.html", '<div class="modal-header dialog-header-notify"><button type="button" class="close" ng-click="close()" class="pull-right" style="font-size:18px;"><i class="glyphicon glyphicon-remove"></i></button><h4 class="modal-title text-info"><span class="icon" >{{"CONTACTUSSUCCESSTITLE" | translate}}</span></h4></div><div class="modal-body text-info" >{{"CONTACTUSSUCCESSMESSAGE" | translate}}</div><div class="modal-footer"><button type="button" class="btn btn-primary" ng-click="close()">{{"OK" | translate}}</button></div>');

        $templateCache.put("/dialogs/videoPreview.html", '<div class="modal-header" style="border-bottom:0px;"><button type="button" class="close" ng-click="close()" style="font-size:18px;" class="pull-right"><i class="glyphicon glyphicon-remove"></i></button></div><div class="modal-body text-info" ><iframe width="100%" height="100%" src="https://www.youtube.com/embed/ybnuWXHsz3g?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe></div>');

        $templateCache.put("/dialogs/videoPreview2.html", '<div class="modal-header" style="border-bottom:0px;"><button type="button" class="close" ng-click="close()" style="font-size:18px;" class="pull-right"><i class="glyphicon glyphicon-remove"></i></button></div><div class="modal-body text-info" ><iframe width="100%" height="100%" src="https://www.youtube.com/embed/ZIx1BGYAnX8?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe></div>');
        /*
           $templateCache.put("/dialogs/videoPreview2.html", '<div class="modal-header" style="border-bottom:0px;"><button type="button" class="close" ng-click="close()" style="font-size:18px;" class="pull-right"><i class="glyphicon glyphicon-remove"></i></button></div><div class="modal-body text-info" ><iframe width="100%" height="100%" src="https://www.youtube.com/embed/KNPGqhrBE7E?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe></div>');
        */
        $templateCache.put("/dialogs/videoPreview3.html", '<div class="modal-header" style="border-bottom:0px;"><button type="button" class="close" ng-click="close()" style="font-size:18px;" class="pull-right"><i class="glyphicon glyphicon-remove"></i></button></div><div class="modal-body text-info" ><iframe width="100%" height="100%" src="https://www.youtube.com/embed/KNPGqhrBE7E?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe></div>');

        $templateCache.put("/dialogs/LinePopup.html", '<div class="modal-header" style="border-bottom:0px;"><button type="button" class="close" ng-click="close()" style="font-size:18px;" class="pull-right"><i class="glyphicon glyphicon-remove"></i></button></div><div class="modal-body text-info" ><div style="text-align: center;"><a href="https://line.me/ti/p/U2ujvjVqD2"> <img src="images/flowacc.png" style="max-width: 135px;"></a> </div><div style="font-size: x-large;text-align: center;"><span>Add</span> <span style="color:#3398cc"> @flowaccount</span> <span> on Line.</span></div><div style="text-align: center;"><a href="https://line.me/ti/p/U2ujvjVqD2"> <img src="images/qrcodeline.JPG"></a> </div><div style="text-align: center;"><a href="https://line.me/ti/p/U2ujvjVqD2"><img src="images/addline.png" style="max-width: 150px;"></a></div></div>');

        $templateCache.put("/dialogs/VidoTutorialQuotation.html", '<div class="modal-header" style="border-bottom:0px;"><button type="button" class="close" ng-click="close()" style="font-size:18px;" class="pull-right"><i class="glyphicon glyphicon-remove"></i></button></div><div class="modal-body text-info" ><iframe width="100%" height="100%" src="https://www.youtube.com/embed/PcTm8G6UjtA?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe></div>');

        $rootScope.$on('$locationChangeStart', function (evt, absNewUrl, absOldUrl) {
          
        });
        $rootScope.$on('$routeChangeStart', function (ev, next, current) {

        });
        $rootScope.$on('$viewContentLoaded', function (ev) {
         
        });
        $rootScope.$on('$routeChangeSuccess', function (ev, next, current) {

            if(next && next.params.langId)
            {
                 var currentPage = next.$$route.originalPath;
                 $rootScope.language = next.params.langId;
                 $rootScope.currentPage = currentPage.substring(9);
                 if(next.params.langId == 'en')
                 {
                        $translate.use('en');
                        angular.element('.language.en').hide();
                        angular.element('.language.th').show();
                        angular.element('.navbar-brand').attr('href', "/en");
                 }
                 else
                 {
                        $translate.use('th');
                        angular.element('.language.th').hide();
                        angular.element('.language.en').show();
                        angular.element('.navbar-brand').attr('href', "/th");
                 }
            }
            else
            {
                 $rootScope.language = 'th';
                 $rootScope.currentPage = '';
                 $translate.use('th');
                  angular.element('.language.th').hide();
                  angular.element('.language.en').show();
                  angular.element('.navbar-brand').attr('href', "/th");
            }
            
        });
       
    }]);
    angularApp.config(["$routeProvider","$translateProvider", "$locationProvider", function ($routeProvider, $translateProvider,  $locationProvider) {

        
        $routeProvider.when('/', {
                       templateUrl: 'landingpage.html',
                       controller: 'HomeController',
                       reloadOnSearch: false
                   }).when('/:langId', {
                       templateUrl: 'landingpage.html',
                       controller: 'HomeController',
                       reloadOnSearch: false
                   }).when('/:langId/kcashcard', {
                       templateUrl: 'k-bank.html',
                       controller: 'KCashBtnController'
                   }).when('/:langId/ais', {
                       templateUrl: 'ais.html',
                       controller: 'KCashBtnController'
                   }).when('/:langId/seminar', {
                       templateUrl: 'seminar.html',
                       controller: 'SeminarController'
                   }).when('/:langId/jobs', {
                       templateUrl: 'job.html',
                       controller: 'JobController'
                   }).when('/:langId/sendmsg', {
                       templateUrl: '/landingpage.html',
                       controller: 'HomeController'
                    }).when('/:langId/tutorials', {
                       templateUrl: 'videos.html',
                       controller: 'VideoController'
                   }).when('/:langId/tutorials/viewvideo/:videoid', {
                       templateUrl: 'viewVideo.html',
                       controller: 'ViewVideoController'
                   }).when('/:langId/faq', {
                       templateUrl: 'faq.html',
                       controller: 'FaqController'
                   }).when('/:langId/accountant', {
                       templateUrl: 'accountant.html',
                       controller: 'AccountantController'
                   }).when('/:langId/privacy-statement', {
                       templateUrl: 'PrivacyAndTermsofUse.html',
                       controller: 'PrivacyController'
                    }).when('/:langId/term-of-use', {
                       templateUrl: 'PrivacyAndTermsofUse.html',
                       controller: 'PrivacyController'
                   }).when('/:langId/pricing', {
                       templateUrl: 'newDesign/pricing-package.html',
                       controller: 'PricingController'
                   }).when('/:langId/kbank60', {
                       templateUrl: 'kbankpage.html',
                       controller: 'KbankPageController'
                   }).otherwise({
                       redirectTo: '/'
                   });

        // Simply register translation table as object hash

         $locationProvider.html5Mode(true);
         $locationProvider.hashPrefix('!');

        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/resources-locale_',
            suffix: '.json'
        });

        //var language = window.navigator.userLanguage || window.navigator.language;
        //language = language.split('-');
        $translateProvider.preferredLanguage('th');
    }]);

// angularApp.use(function(req, res) {
//     res.sendfile(__dirname + '/index.html');
// });

}).call(this),
(function () {
    "use strict";
    angular.module("app.localization", []).factory("localize", ["$http", "$rootScope", "$window",
        function ($http, $rootScope, $window) {
            var localize;
            return localize = {
                language: "",
                url: void 0,
                resourceFileLoaded: !1,
                successCallback: function (data) {
                    return localize.dictionary = data, localize.resourceFileLoaded = !0, $rootScope.$broadcast("localizeResourcesUpdated")
                },
                setLanguage: function (value) {
                    return localize.language = value.toLowerCase().split("-")[0], localize.initLocalizedResources()
                },
                setUrl: function (value) {
                    return localize.url = value, localize.initLocalizedResources()
                },
                buildUrl: function () {
                    return localize.language || (localize.language = ($window.navigator.userLanguage || $window.navigator.language).toLowerCase(), localize.language = localize.language.split("-")[0]), "i18n/resources-locale_" + localize.language + ".js"
                },
                initLocalizedResources: function () {
                    var url;
                    return url = localize.url || localize.buildUrl(), $http({
                        method: "GET",
                        url: url,
                        cache: !1
                    }).success(localize.successCallback).error(function () {
                        return $rootScope.$broadcast("localizeResourcesUpdated")
                    })
                },
                getLocalizedString: function (value) {
                    var result, valueLowerCase;
                    return result = void 0, localize.dictionary && value ? (valueLowerCase = value.toLowerCase(), result = "" === localize.dictionary[valueLowerCase] ? value : localize.dictionary[valueLowerCase]) : result = value, result
                }
            }
        }
    ]).directive("i18n", ["localize",
        function (localize) {
            var i18nDirective;
            return i18nDirective = {
                restrict: "EA",
                updateText: function (ele, input, placeholder) {
                    var result;
                    return result = void 0, "i18n-placeholder" === input ? (result = localize.getLocalizedString(placeholder), ele.attr("placeholder", result)) : input.length >= 1 ? (result = localize.getLocalizedString(input), ele.html(result)) : void 0
                },
                link: function (scope, ele, attrs) {
                    return scope.$on("localizeResourcesUpdated", function () {
                        return i18nDirective.updateText(ele, attrs.i18n, attrs.placeholder)
                    }), attrs.$observe("i18n", function (value) {
                        return i18nDirective.updateText(ele, value, attrs.placeholder)
                    })
                }
            }
        }
    ]).controller("LangCtrl", ["$scope", "localize", "$translate", "$timeout",
    function ($scope, localize, $translate, $timeout) {
        $scope.lang = $translate.use();

        $(window).load(function () {
            var text = $translate.use() == 'en' ? angular.element('.language.th').show() : angular.element('.language.en').show();
            //$timeout(function () {
          
            //    var text = $translate.use() == 'en' ? angular.element('.language.th').show() : angular.element('.language.en').show();
            //    console.log('aaa');
            //});
        });
        return $scope.setLang = function (lang) {
            
                $scope.lang = lang;
                switch (lang) {
                    case "en":
                        $translate.use('en');
                        angular.element('.language.en').hide();
                        angular.element('.language.th').show();
                        angular.element('.navbar-brand').attr('href', "/en");
                        //localize.setLanguage("EN-US");
                        break;
                    case "th":
                        $translate.use('th');
                        angular.element('.language.th').hide();
                        angular.element('.language.en').show();
                         angular.element('.navbar-brand').attr('href', "/th");
                        //localize.setLanguage("TH-TH");
                        break;
                    default:
                        //localize.setLanguage("EN-US");
                        break;
                }
                //return $scope.lang = lang
            }
        }
    ])

    angular.module("app.frontend", ["ui.bootstrap", "dialogs.controllers", "dialogs.services"]).factory("ContactUsService", ['dialogs', function (dialogs) {
        var contactserv
        return contactserv = {
            success: function () {
                dialogs.create('/dialogs/notify1.html', "notifyDialogCtrl", {}, {});

            }
        }
    }]).directive("contactinit", ["ContactUsService", "$location", "$templateCache",
        function (ContactUsService, $location, $templateCache) {
            var cinitDirective;
            return cinitDirective = {
                link: function (scope, ele, attrs) {

                    if ($location.search().status == "success") {
                        ContactUsService.success();
                    }
                }
            }
        }
    ]).controller("VideoPreviewCtrl", ["$scope", "dialogs", "$translate", "$timeout",
        function ($scope, dialogs, $translate, $timeout) {
            $scope.open = function () {
                dialogs.create('/dialogs/videoPreview.html', "notifyDialogCtrl", {}, {});

                $timeout(function () {
                    if ($(window).width > 1000) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '443px');
                    }
                    else if ($(window).width() > 767) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '492px');
                    }
                    else {
                        $('.modal-dialog').css('width', '95%');
                        $('.modal-dialog').css('margin-left', 'auto');
                        $('.modal-dialog').css('margin-right', 'auto');
                        $('.modal-dialog').css('max-width', '640px');
                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '372px');
                    }
                });
            }
        }
    ]).controller("VideoPreview2Ctrl", ["$scope", "dialogs", "$translate", "$timeout",
        function ($scope, dialogs, $translate, $timeout) {
            $scope.open = function () {
                dialogs.create('/dialogs/videoPreview2.html', "notifyDialogCtrl", {}, {});

                $timeout(function () {
                    if ($(window).width > 1000) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '443px');
                    }
                    else if ($(window).width() > 767) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '492px');
                    }
                    else {
                        $('.modal-dialog').css('width', '95%');
                        $('.modal-dialog').css('margin-left', 'auto');
                        $('.modal-dialog').css('margin-right', 'auto');
                        $('.modal-dialog').css('max-width', '640px');
                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '372px');
                    }
                });
            }
        }
    ]).controller("VideoPreview3Ctrl", ["$scope", "dialogs", "$translate", "$timeout",
        function ($scope, dialogs, $translate, $timeout) {
            $scope.open = function () {
                dialogs.create('/dialogs/videoPreview3.html', "notifyDialogCtrl", {}, {});

                $timeout(function () {
                    if ($(window).width > 1000) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '443px');
                    }
                    else if ($(window).width() > 767) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '492px');
                    }
                    else {
                        $('.modal-dialog').css('width', '95%');
                        $('.modal-dialog').css('margin-left', 'auto');
                        $('.modal-dialog').css('margin-right', 'auto');
                        $('.modal-dialog').css('max-width', '640px');
                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '372px');
                    }
                });
            }
        }
    ]).controller("LinePopupCtrl", ["$scope", "dialogs", "$translate", "$timeout",
        function ($scope, dialogs, $translate, $timeout) {
            $scope.open = function () {
                dialogs.create('/dialogs/LinePopup.html', "notifyDialogCtrl", {}, {});

                $timeout(function () {
                    if ($(window).width > 1000) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '400px');
                        $('.modal-body').css('max-width', '400px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '500px');
                    }
                    else if ($(window).width() > 767) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '400px');
                        $('.modal-body').css('max-width', '400px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '500px');
                    }
                    else {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('margin-left', 'auto');
                        $('.modal-dialog').css('margin-right', 'auto');
                        $('.modal-dialog').css('max-width', '400px');
                        //var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '568px');
                    }
                });
            }
        }
    ]).controller("VidoTutorialQuotation", ["$scope", "dialogs", "$translate", "$timeout",
        function ($scope, dialogs, $translate, $timeout) {
            $scope.open = function () {
                dialogs.create('/dialogs/VidoTutorialQuotation.html', "notifyDialogCtrl", {}, {});

                $timeout(function () {
                    if ($(window).width > 1000) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '443px');
                    }
                    else if ($(window).width() > 767) {
                        $('.modal-dialog').css('width', '100%');
                        $('.modal-dialog').css('max-width', '853px');
                        $('.modal-body').css('max-width', '853px');

                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '492px');
                    }
                    else {
                        $('.modal-dialog').css('width', '95%');
                        $('.modal-dialog').css('margin-left', 'auto');
                        $('.modal-dialog').css('margin-right', 'auto');
                        $('.modal-dialog').css('max-width', '640px');
                        var height = $(window).height() - ((20 * $(window).height()) / 100);
                        $('.modal-body').css('height', height);
                        $('.modal-body').css('max-height', '372px');
                    }
                });
            }
        }
    ]);

}).call(this)

