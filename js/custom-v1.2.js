﻿angular.module('app.frontend').service('MetadataService', ['$window', function($window){
 var self = this;
 self.setMetaTags = function (tagData){
   $('meta[name="title"]').attr('content',tagData.title);
   $('title').html(tagData.title);
   $('meta[name="description"]').attr('content',tagData.description);
   // $('meta').attr('content',tagData.description);
 }; 
}]);
angular.module("app.frontend").controller("NavBarCtrl", ["$scope", "dialogs", "$translate", "$timeout", "$cookies", "$document",
    function($scope, dialogs, $translate, $timeout, $cookies, $document) {

             $scope.toTheTop = function() {
                $document.scrollTopAnimated(0, 2000).then(function() {
              });
            }

    }]).controller("HomeController", ["$scope", "dialogs", "$translate", "$timeout", "$cookies", "cookieStore", "$rootScope", "$location",
    function($scope, dialogs, $translate, $timeout, $cookies, cookieStore, $rootScope, $location) {

        // console.log(window.location.href);

        var str = window.location.href;
        var res = str.split("com/", 2);
        // console.log(res[1]);
        // $timeout(function(){
        var myDate = new Date(new Date().getTime() + (30 * 24 * 60 * 60 * 1000));
       //console.log(myDate);
        if (res[1] != undefined) {
            if (res[1].length == '4' && res[1] == res[1].toUpperCase()) {

                var _firstseen = cookieStore.get("FirstSeen");

                if (_firstseen == null) {
                    cookieStore.put("ReferralCode", res[1], {
                        end: myDate,
                        domain: '.flowaccount.com'
                    });
                    /* do action */
                    var _form = $("<form />");
                    _form.attr('id', "referralForm");
                    _form.attr('method', "POST");
                    _form.attr('target', "_self");
                    $('body').append(_form);
                    _form.ajaxSubmit({
                        url: 'https://app.flowaccount.com/Account/AddReferralConversion',
                        type: 'post',
                        xhrFields: {
                            withCredentials: true
                        },
                        success: console.log('success')
                    });
                    cookieStore.put("FirstSeen", '1', {
                        end: Infinity,
                        domain: '.flowaccount.com'
                    });
                    _form.remove();

                }
            }
        }

        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(true, $rootScope);
            menuBar();
            $('#homemenuli').hide();
            $('#menuli').show();
            $('.padd-nav').show();
            $('.container-contact').show();
            $scope.gotopricing = function(){

                var url = "/" + $rootScope.language + "/pricing";
                              $location.path(url);
                //window.location.href = url;
            };
            // $('a[href*="#"]:not([href="#"]):not([class="accordion-toggle"])').click(function() {
            //     if (!$(this).hasClass('fancybox')) {
            //         if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            //             var target = this.hash;
            //             target = $('#' + this.hash.slice(1));
            //             if (target.length > 0) {
            //                 $('html,body').animate({
            //                     scrollTop: (target.offset().top - 49)
            //                 });
            //                 return false;
            //             }
            //         }
            //     }
            // });

            $('.flow-validate').each(function(index) {
                var $input = $(this);
                $input.blur(function() {
                    if ($(this).val() != "") {
                        $(this).parent().removeClass('has-error');
                    }
                });
            });

            $('.flow-register-validate').each(function(index) {
                var $input = $(this);
                $input.blur(function() {
                    if ($(this).val() != "") {
                        $(this).parent().removeClass('has-error');
                    }
                });
            });
            var parser = new UAParser();
            var os = parser.getResult().os;
            var device = parser.getResult().device;


            $('#watch-overview-youtube').fadeIn(function() {
                if ((os.name == "iOS") || (os.name == "Android")) { //    $('.deviceShow').show();

                    $('#videoFlow')[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
                } else {
                    gaTrackEvent('Video', 'View', 'FlowAccount Overview');
                    $('#videoFlow')[0].contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
                }
            });

            //                  });
            //       })

            // Register


            $('.squaredFour label').click(function() {
                if ($('#squaredFour').val() == 'true') {
                    $('#squaredFour').val(false);
                    $('label[for="squaredFour"]').css('border', "2px solid red");
                } else {
                    $('#squaredFour').val(true);
                    $('label[for="squaredFour"]').css('border', "0px solid red");
                }
            })


            function validateEmail(email) {
                var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9]+([\.|\-][a-zA-Z0-9]+)*(\.[a-zA-Z]{1,61})$/;
                return re.test(email);
            }
            $('.registerbtn').click(function() {
                // var dataregister = $('form#registerForm').serialize();
                if ($('#squaredFour').val() == 'true' && $('#input-email').val() != "" && validateEmail($('#input-email').val()) == true && $('#input-password').val() != "" && $('#input-password').val().length >= 6) {
                    var isBreaks = false;
                    $('label[for="squaredFour"]').css('border', "0px solid red");
                    gaTrackEvent('button', 'register', 'registerForm Frontend');
                    mixpanel.track("Register On Landing Page");
                    $('.flow-register-validate').parents('form').submit();
                } else {
                    if ($('#squaredFour').val() != 'true') {
                        $('label[for="squaredFour"]').css('border', "2px solid red");
                    } else {
                        $('label[for="squaredFour"]').css('border', "0px solid red");
                    }

                    $('.flow-register-validate').each(function(index) {
                        var $input = $(this);
                        if ($('#input-email').val() == "") {
                            $('#email-register').removeClass('hidden');
                            $('#email-register').html($translate.instant("ValidateEmailEmpty"));
                            $('#input-email').addClass('has-error');
                        } else {
                            if (validateEmail($('#input-email').val()) == true) {
                                $('#email-register').addClass('hidden');
                                $('#input-email').removeClass('has-error');
                            } else {
                                $('#email-register').removeClass('hidden');
                                $('#email-register').html($translate.instant("ValidateEmailWrong"));
                                $('#input-email').addClass('has-error');
                            }
                        }

                        if ($('#input-password').val() == "") {
                            $('#password-register').removeClass('hidden');
                            $('#password-register').html($translate.instant("ValidatePasswordEmpty"));
                            $('#input-password').addClass('has-error');
                        } else {
                            if ($('#input-password').val().length < 6) {
                                $('#password-register').removeClass('hidden');
                                $('#password-register').html($translate.instant("ValidatePasswordWrong"));
                                $('#input-password').addClass('has-error');
                            } else {
                                $('#password-register').addClass('hidden');
                                $('#input-password').removeClass('has-error');
                            }
                        }
                    });

                    return false;
                }


            })

            // end register

            // SEND MAIL 
            $('.flow-btn-submit').click(function() {

                if ($('.flow-btn-submit').hasClass('mail-success')) {
                    $('#p1').addClass('hidden');
                    return;
                }

                var data = $('form#mail').serialize();
                var isBreak = false;
                $('.flow-validate').each(function(index) {

                    var $input = $(this);
                    if ($input.val() == "") {
                        $input.parent().addClass('has-error');
                        $input.focus();
                        isBreak = true;

                        if ($('#name').val() == "") {
                            $('#p1').removeClass('hidden');
                        } else {
                            $('#p1').addClass('hidden');
                        }
                        if ($('#email').val() == "") {
                            $('#p2').removeClass('hidden');
                        } else {
                            $('#p2').addClass('hidden');
                        }
                        if ($('#subject').val() == "") {
                            $('#p3').removeClass('hidden');
                        } else {
                            $('#p3').addClass('hidden');
                        }
                        if ($('#message').val() == "") {
                            $('#p4').removeClass('hidden');
                        } else {
                            $('#p4').addClass('hidden');
                        }
                        return false;

                    }
                });

                if (isBreak)
                    return;

                $('.flow-btn-submit').parents('form').submit();

            });
            $('#exportpdfbtn').on('click', function() {


                $('.popover').remove();
                if ($('#invoiceForm').valid()) {
                    var scope = angular.element($(".invoice-page")).scope();
                    scope.open('SaveFreeInvoiceCtrl', 'ModalContent.html');
                }
            });

            $('.btn-toggle-cover-close').click(function() {

                $('.trowser').removeClass('slide-down');
                $('.trowser').toggleClass('slide-up');
                $timeout(function() {
                        $('.trowser').toggleClass('slide-up');
                        $('.shell').toggleClass('trowserView');
                        $('.trowser').find('.invoice-container .web-view').html('');
                        $('body').css('overflow', 'auto');
                    }

                    , 500);
            });

            $timeout(function() {
                $('.videoFlow').click(function() {
                        if ($(window).width() > 1024) {
                            $('section#banner').addClass('disable');
                        }
                    })
                    // event that will be fired when the player is fully loaded
                    //onYouTubePlayerReady('vS8EdXO09zs');
                var ytplayer = new YT.Player('videoFlow', {
                    events: {
                        'onReady': onYouTubePlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });

                function onYouTubePlayerReady(pid) {
                        //ytplayer = document.getElementById("videoFlow");

                    }
                    // event that will be fired when the state of the player changes
                function onPlayerStateChange(state) {
                    // check if it's playing
                    if (state.data == 1) {
                       //console.log("is playing");
                        if ($(window).width() > 1024) {
                            $('section#banner').addClass('disable')
                            gaTrackEvent('Video', 'View', 'FlowAccount Overview');
                        } else {
                            gaTrackEvent('Video', 'View', 'FlowAccount Overview');
                        }
                        // is playing
                    }
                }

            });

        });


    }
]).controller("PriceAccordionCtrl", ["$scope", "dialogs", "$translate", "$timeout",
    function($scope, dialogs, $translate, $timeout) {
        //$scope.oneAtATime = true;

        $scope.status = {
            open: true,
            open2: true,
            open3: true,
            open4: true,
        };

        $scope.cssclass = {
                free: true
            }

        $scope.statusPricingPage = {
open: false,
            open2: true,
            open3: false,
            open4: false,
        };
            //$timeout(function () {
            //    angular.element('#price-accordion').find('.panel-heading').on('click', function (e) {

        //        //Open Close accordion .....
        //        //angular.element(this).scope().toggleOpen();
        //    })
        //});

    }
]).controller("KCashBtnController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {

        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Promote"),
               description: $translate.instant("metadescription-Promote")
            });
        });

    }
]).controller("SeminarController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {

        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Promote"),
               description: $translate.instant("metadescription-Promote")
            });

            loadVideos();
            $(this).scrollTop('0');
        });

    }
]).controller("JobController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {
        
        $(window).resize(function() {
            $scope.getimage();
        });
        $scope.getimage = function(){            
            if ($(window).width() > 1440) {
                $('.job-image').attr('src','images/job/img_jobs_groupshotFlowAccount_2560.jpg');
            }
            else{
                $('.job-image').attr('src','images/job/img_jobs_groupshotFlowAccount_1440.jpg');                
            }
        }
        $scope.getimage();
        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Promote"),
               description: $translate.instant("metadescription-Promote")
            });

            $(this).scrollTop('0');

        });

    }
]).controller("FaqController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {

        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Faq"),
               description: $translate.instant("metadescription-Faq")
            });
        });

    }
]).controller("PrivacyController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {

        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Privacy"),
               description: $translate.instant("metadescription-Privacy")
            });
          });

        $scope.getw = $(window).width();
        if ($scope.getw < 768) {
            $('#nav-pill').removeClass('nav-stacked');
            $('#nav-pill').addClass('nav-justified');
            $('#header-title').css('width', '100%');
            $('#header-title2').css('width', '100%');
            $('#tab1').css('width', '100%');
        }
        $(window).on('resize', function () {
            $scope.getw = $(window).width();
            if ($scope.getw < 768) {
                $('#nav-pill').removeClass('nav-stacked');
                $('#nav-pill').addClass('nav-justified');
                $('#tab1').css('width', '100%');
                $('#header-title').css('width', '100%');
                $('#header-title2').css('width', '100%');
            }
            else {
                $('#nav-pill').addClass('nav-stacked');
                $('#nav-pill').removeClass('nav-justified');
                $('#header-title').css('width', '');
                $('#header-title2').css('width', '100%');
                $('#tab1').css('width', '');
            }
        });
        $('#li-TermofUse').click(function () {
            $('#header-title').html('เงื่อนไขการใช้บริการ (Terms of Use)');
            $('#header-title2').html('เงื่อนไขการใช้บริการ (Terms of Use)');
        });
        $('#li-Privacy').click(function () {
            $('#header-title').html('นโยบายคุ้มครองความเป็นส่วนตัว (Privacy Policy)');
            $('#header-title2').html('นโยบายคุ้มครองความเป็นส่วนตัว (Privacy Policy)');
        });
        //console.log(window.location.href.indexOf("TermofUse"));
        if (window.location.href.indexOf("term-of-use") > -1) {
            $('#header-title').html('เงื่อนไขการใช้บริการ (Terms of Use)');
            $('#header-title2').html('เงื่อนไขการใช้บริการ (Terms of Use)');
            $('#li-Privacy').removeClass('active');
            $('#li-TermofUse').addClass('active');
            $('#Privacy').removeClass('active');
            $('#TermofUse').addClass('active');
           //console.log('TermofUse');
        }
        else if (window.location.href.indexOf("Privacy") > -1) {
            $('#header-title').html('นโยบายคุ้มครองความเป็นส่วนตัว (Privacy Policy)');
            $('#header-title2').html('นโยบายคุ้มครองความเป็นส่วนตัว (Privacy Policy)');
            $('#li-TermofUse').removeClass('active');
            $('#li-Privacy').addClass('active');
            $('#TermofUse').removeClass('active');
            $('#Privacy').addClass('active');
           //console.log('Privacy');
        }
        $(".navbar li a").click(function (event) {
            // check if window is small enough so dropdown is created
            $(".navbar-collapse").removeClass("in").addClass("collapse");
        }       );
        $("#cols1").click(function () {
            $("#cols1").addClass('hidden').addClass('absol');
            $("#cols2").removeClass('hidden').removeClass('absol');
        });
        $("#cols2").click(function () {
            $("#cols1").removeClass('hidden');
            $("#cols2").addClass('hidden');
        });
        $scope.menuDisplayTimeout;
        $('#menu').hover(function () {
            $timeout.cancel($scope.menuDisplayTimeout);
            $("#menubar").fadeIn('fast');
        },function () {
            $scope.menuDisplayTimeout = $timeout(function () {
                $("#menubar").fadeOut('fast');
            },200);
        });
        $('#menubar').hover(function () {
            $timeout.cancel($scope.menuDisplayTimeout);
        },function () {
            $scope.menuDisplayTimeout = $timeout(function () {
                $("#menubar").fadeOut('fast');
            },200);
        });

    }
]).controller("AccountantController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {

        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Accountant"),
               description: $translate.instant("metadescription-Accountant")
            });

            loadVideos();
        });

    }
]).controller("VideoController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService,$routeParams) {
        $timeout(function() {
            initHeaderMenu();
            switchPricingMenu(false, $rootScope);

            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Video"),
               description: $translate.instant("metadescription-Video")
            });

            loadVideos();
        });


    }
]).controller("ViewVideoController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService", "$routeParams",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService, $routeParams) {
        if (typeof $routeParams.videoid === 'undefined')
            return false;

        $scope.videoId = $routeParams.videoid;
        $scope.title = "Videokk"
        
        $timeout(function() {
            initHeaderMenu();
            showSignupAndLang(true);   
            switchPricingMenu(false, $rootScope);
            showSignupAndLang(true);   

            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle-Video"),
               description: $translate.instant("metadescription-Video")
            });

            loadVideos();
        });


    }
]).controller("KbankPageController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function($scope, dialogs, $translate, $timeout, $rootScope,MetadataService) {
            
        $timeout(function() {

            initHeaderMenu();
            showSignupAndLang(false);   
            switchPricingMenu(false, $rootScope);
            //  change meta
            MetadataService.setMetaTags({
               title: $translate.instant("metatitle"),
               description: $translate.instant("metaDescription")
            });
        });

        // Register


            $('.squaredFour label').click(function() {
                if ($('#squaredFour').val() == 'true') {
                    $('#squaredFour').val(false);
                    $('label[for="squaredFour"]').css('border', "2px solid red");
                } else {
                    $('#squaredFour').val(true);
                    $('label[for="squaredFour"]').css('border', "0px solid red");
                }
            })


            function validateEmail(email) {
                var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9]+([\.|\-][a-zA-Z0-9]+)*(\.[a-zA-Z]{1,61})$/;
                return re.test(email);
            }
            $('.registerbtn').click(function() {
               //console.log( $('#input-contact').val());
                // var dataregister = $('form#registerForm').serialize();
                if ($('#squaredFour').val() == 'true' && $('#input-email').val() != "" && validateEmail($('#input-email').val()) == true && $('#input-password').val() != "" && $('#input-password').val().length >= 6
                    && $('#input-contact').val() != "" && $('#input-contact').val().length == 12) {
                    var isBreaks = false;
                    $('label[for="squaredFour"]').css('border', "0px solid red");
                    gaTrackEvent('button', 'register', 'registerForm Frontend');
                    mixpanel.track("Register On Landing Page");
                    $('.flow-register-validate').parents('form').submit();
                } else {
                    if ($('#squaredFour').val() != 'true') {
                        $('label[for="squaredFour"]').css('border', "2px solid red");
                    } else {
                        $('label[for="squaredFour"]').css('border', "0px solid red");
                    }

                    $('.flow-register-validate').each(function(index) {
                        var $input = $(this);
                        if ($('#input-email').val() == "") {
                            $('#email-register').removeClass('hidden');
                            $('#email-register').html($translate.instant("ValidateEmailEmpty"));
                            $('#input-email').addClass('has-error');
                        } else {
                            if (validateEmail($('#input-email').val()) == true) {
                                $('#email-register').addClass('hidden');
                                $('#input-email').removeClass('has-error');
                            } else {
                                $('#email-register').removeClass('hidden');
                                $('#email-register').html($translate.instant("ValidateEmailWrong"));
                                $('#input-email').addClass('has-error');
                            }
                        }

                        if ($('#input-password').val() == "") {
                            $('#password-register').removeClass('hidden');
                            $('#password-register').html($translate.instant("ValidatePasswordEmpty"));
                            $('#input-password').addClass('has-error');
                        } else {
                            if ($('#input-password').val().length < 6) {
                                $('#password-register').removeClass('hidden');
                                $('#password-register').html($translate.instant("ValidatePasswordWrong"));
                                $('#input-password').addClass('has-error');
                            } else {
                                $('#password-register').addClass('hidden');
                                $('#input-password').removeClass('has-error');
                            }
                        }
                    });

                        if ($('#input-contact').val() == "") {
                            $('#contact-register').removeClass('hidden');
                            $('#contact-register').html($translate.instant("ValidateContactEmpty"));
                            $('#input-contact').addClass('has-error');
                        } else {
                            if ($('#input-contact').val().length < 6) {
                                $('#contact-register').removeClass('hidden');
                                $('#contact-register').html($translate.instant("ValidateContactEmpty"));
                                $('#input-contact').addClass('has-error');
                            } else {
                                $('#contact-register').addClass('hidden');
                                $('#input-contact').removeClass('has-error');
                            }
                        }

                    return false;
                }


            })

            // end register

    }
]).controller("PricingController", ["$scope", "dialogs", "$translate", "$timeout", "$rootScope","MetadataService",
    function ($scope, dialogs, $translate, $timeout, $rootScope, MetadataService) {

        $timeout(function () {
            initHeaderMenu();
            showSignupAndLang(true);
            switchPricingMenu(false, $rootScope);

            //  change meta
            MetadataService.setMetaTags({
                title: $translate.instant("metatitle-Pricing"),
                description: $translate.instant("metadescription-Pricing")
            });

            var _boxBasic = $('section#pricing .box-basic .col-content');
            var _boxPremium = $('section#pricing .box-premium .col-content');
            var _boxPremiumPro = $('section#pricing .box-premium165 .col-content');
            var _boxPremiumPlus = $('section#pricing .box-premium330 .col-content');

            _boxBasic.hover(function () {
                $(this).find('.box-more').toggleClass('hovered');
            });

            _boxPremium.hover(function () {
                $(this).find('.box-more').toggleClass('hovered');
            });

            _boxPremiumPro.hover(function () {
                $(this).find('.box-more').toggleClass('hovered');
            });

            _boxPremiumPlus.hover(function () {
                $(this).find('.box-more').toggleClass('hovered');
            });

        });
        $scope.toggleshow = function (data) {
            var _detail = 'section#pricing .box-' + data;

            if ($(_detail).find('.box-detail .btn:hover').length > 0) {
                return;
            }

            $(_detail).find('.body-list').slideToggle('fast');
            $(_detail).find('.box-more i').toggleClass('fa-angle-down');
        }

    }]);

var switchPricingMenu = function(homePage, $rootScope)
{
    if(homePage == true)
    {
         // angular.element('.pricing-link').attr('href', "/"+$rootScope.language+'#pricing');
         angular.element('.contact-link').attr('href', "/"+$rootScope.language+'#contactus');
         $('div.content__more-arrow > a').attr('href', "/"+$rootScope.language+'#features');
    }
    else
    {
         // angular.element('.pricing-link').attr('href', "/"+$rootScope.language+'#pricing');
         angular.element('.contact-link').attr('href', "/"+$rootScope.language+'#contactus');
         // $('div.content__more-arrow > a').attr('href', "/"+$rootScope.language+'#features');
         window.scrollTo(0, 0);
    }
}
var showSignupAndLang = function(IsShow){
    if(!IsShow){
        $('.btn-signup-head').hide();
    }else{
        $('.btn-signup-head').show();
    }
}
var initHeaderMenu = function() {
    var str = window.location.href;
    angular.element(document).off('scroll');
    // MENU STICK
    angular.element(document).on('scroll', setMenuFixedStatic);
    // SMOOTH SCROLL
    setMenuFixedStatic();

};
var setMenuFixedStatic = function(e) {

    var _featuresSection = $('section#features').offset().top - $('.navbar').height();
    // var _overviewSection = $('section#overview').offset().top - $('.navbar').height();
    var scrollTop = $(this).scrollTop();
    var $nav = $('.menu');
    if (scrollTop >= _featuresSection) {
        var $nav = $('.menu');
        if ($nav.hasClass('navbar-fixed-top')) {
            return;
        }
        var wlogo = 321;
        $nav.removeClass('navbar-static-top').addClass('navbar-fixed-top');

        $nav.find('.navbar').find('ul.nav li:last-child').show();

        $('img.logo').width(wlogo);
        //$('body').addClass('bodymargin');
        setTimeout(function() {
            $nav.find('.navbar').addClass('show-down');

        }, 50);
        /*
        $nav.stop().animate({
            'overflow': 'auto',
            'min-height': 50,
            'height': 50,
        }, 300);
        */
        if ($('section#banner').hasClass('disable')) {

            $('.banner-content.headpromote').addClass('overview-alwaystop');
            $('#videoFlow').addClass('iframeoverview');

        }
    } else {
        var $nav = $('.menu');
        if ($nav.hasClass('navbar-static-top')) {
            return;
        }

        $nav.find('.navbar').removeClass('show-down');
        setTimeout(function() {
            //$('body').removeClass('bodymargin');

            // $nav.find('.navbar').find('ul.nav li:last-child').hide(); // for hide register when top
            $nav.removeClass('navbar-fixed-top').addClass('navbar-static-top');

            if ($('section#banner').hasClass('disable')) {
                $('.banner-content.headpromote').fadeOut('fast', function() {
                    $('.banner-content.headpromote').removeClass('overview-alwaystop');
                    $('#videoFlow').removeClass('iframeoverview');
                    $('.banner-content.headpromote').fadeIn('fast');
                });
            }
        }, 250);

    }

};

var menuBar = function() {
    $(".navbar li a").click(function(event) {

        // check if window is small enough so dropdown is created
        $(".navbar-collapse").removeClass("in").addClass("collapse");
    });
    $("#cols1").click(function() {
        $("#cols1").addClass('hidden').addClass('absol');
        $("#cols2").removeClass('hidden').removeClass('absol');

    });
    $("#cols2").click(function() {
        $("#cols1").removeClass('hidden');
        $("#cols2").addClass('hidden');
    });

    var menuDisplayTimeout;
    $('#menu').hover(function() {
        clearTimeout(menuDisplayTimeout);
        $("#menubar").fadeIn('fast');
    }, function() {
        menuDisplayTimeout = setTimeout(function() {
            $("#menubar").fadeOut('fast');
        }, 400);
    });

    $('#menubar').hover(function() {
        clearTimeout(menuDisplayTimeout);
    }, function() {
        menuDisplayTimeout = setTimeout(function() {
            $("#menubar").fadeOut('fast');
        }, 400);
    });
};
$(document).ready(function() {
    $('.navbar-default .navbar-nav > li > a').click(function() {
        $('.navbar-default .navbar-nav > li > a').removeClass('active');
        $(this).addClass('active');
    });
});

function gaTrackEvent(category, action, label) {
    ga('send', 'event', {
        eventCategory: category,
        eventAction: action,
        eventLabel: label
    });

}

window.mobileAndTabletcheck = function() {
    var check = false;
    (function(a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}

function loadVideos() {
    // console.log(window.mobileAndTabletcheck());
    if (!window.mobileAndTabletcheck()) {
        if (!document.getElementsByClassName) {
            // IE8 support
            var getElementsByClassName = function(node, classname) {
                var a = [];
                var re = new RegExp('(^| )' + classname + '( |$)');
                var els = node.getElementsByTagName("*");
                for (var i = 0, j = els.length; i < j; i++)
                    if (re.test(els[i].className)) a.push(els[i]);
                return a;
            }
            var videos = getElementsByClassName(document.body, "youtube");
        } else {
            var videos = document.getElementsByClassName("youtube");
        }
        var nb_videos = videos.length;
        for (var i = 0; i < nb_videos; i++) {
            // Based on the YouTube ID, we can easily find the thumbnail image
            videos[i].style.backgroundImage = 'url(https://i.ytimg.com/vi/' + videos[i].id + '/maxresdefault.jpg)';

            // Overlay the Play icon to make it look like a video player
            var play = document.createElement("div");
            play.setAttribute("class", "play");
            videos[i].appendChild(play);

            videos[i].onclick = function() {
                $(this).fadeOut('fast', function() {
                    // Create an iFrame with autoplay set to true
                    var iframe = document.createElement("iframe");
                    var iframe_url = "https://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1&rel=0";

                    if (this.getAttribute("data-params")) iframe_url += '&' + this.getAttribute("data-params");
                  
                    iframe.setAttribute("class","embed-responsive-item");
                    iframe.setAttribute("src", iframe_url);
                    iframe.setAttribute("frameborder", '0');
                    iframe.setAttribute("allowfullscreen", '');

                    if(this.getAttribute('class') == 'youtube vdo big' && $(window).width() > 671){
                       
                        iframe.setAttribute("width", '420');
                        iframe.setAttribute("height", '237');
                    }
                    else{
                        iframe.setAttribute("width", '300');
                        iframe.setAttribute("height", '169');
                    }
                    
                    if(this.getAttribute('class') == 'youtube vdo big seminar' && $(window).width() > 671){
                        iframe.setAttribute("width", '640');
                        iframe.setAttribute("height", '360');
                    }
                    iframe.setAttribute("id", this.id);

                    // The height and width of the iFrame should be the same as parent
                    iframe.style.width = this.style.width;
                    iframe.style.height = 169;
                    // Replace the YouTube thumbnail with YouTube Player
                    this.parentNode.replaceChild(iframe, this);
                    setTimeout(function() {
                        // onYouTubePlayerAPIReady(iframe.getAttribute('id'),300);
                    }, 3000)
                });
            }
        }
    } else {
        var videos = $(".youtube.vdo");
        var nb_videos = videos.length;
        loadMobileVideo(videos[0], 0, videos);

    }



}

function loadMobileVideo(video, index, videos) {
    var iframe = document.createElement("iframe");
    var iframe_url = "https://www.youtube.com/embed/" + video.id + "?autohide=1";
    if (video.getAttribute("data-params")) iframe_url += '&' + video.getAttribute("data-params");
    iframe.setAttribute("src", iframe_url);
    iframe.setAttribute("frameborder", '0');
    iframe.setAttribute("allowfullscreen", '');
    if (video.getAttribute('class') == 'youtube vdo big' && $(window).width() > 671) {
        iframe.setAttribute("width", '420');
        iframe.setAttribute("height", '236');
        iframe.style.height = 236;
    } else {
        iframe.setAttribute("width", '300');
        iframe.setAttribute("height", '169');
        iframe.style.height = 169;
    }

    if (video.getAttribute('class') == 'youtube vdo big seminar' && $(window).width() > 671) {
        iframe.setAttribute("width", '640');
        iframe.setAttribute("height", '360');
        iframe.style.height = 360;
    }

    iframe.setAttribute("id", video.id);
    // The height and width of the iFrame should be the same as parent
    iframe.style.width = video.style.width;
    // Replace the YouTube thumbnail with YouTube Player
    video.parentNode.replaceChild(iframe, video);


    // iframe.setAttribute("onclick","gaTrackEvent('Video', 'View',' " + video.getAttribute('name') + " )" );
    $(iframe).load(function() {
        // do stuff 
        if ((videos.length - 1) > index) {
            loadMobileVideo(videos[index + 1], (index + 1), videos);
        }
    });
}

/*


                setTimeout(function() {
                    var ytplayer = new YT.Player(video.id, {
                        events: {
                            'onReady': onYouTubePlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                    });

                    function onYouTubePlayerReady(pid) {
                            //ytplayer = document.getElementById("videoFlow");

                        }
                        // event that will be fired when the state of the player changes
                    function onPlayerStateChange(state) {
                       console.log('come');
                        // check if it's playing
                        if (state.data == 1) {
                           console.log("is playing");
                                gaTrackEvent('Video', 'View', video.getAttribute('name'));
                            // is playing
                        }
                    }

                });

            */